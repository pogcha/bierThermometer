# -*- coding: utf-8 -*-
"""
Created on Mon Jan  8 20:05:18 2018

@author: malte
"""

import matplotlib.pyplot as plt
import os
import time
import locale

degree_sign= u'\N{DEGREE SIGN}'

def init(exe,port,refresh):
    # initlize the digitemp software
    os.system('{0:} -s{1:} -i -r{2:d}'.format(exe,port,refresh))    

    
def initSaveData():
    # initialize a file to save the measured data into
    filename = time.strftime('%Y-%b-%d_%H-%M-%S').decode(locale.getlocale()[1])
    filename = 'TempMessung'+filename+'.dat'
    print 'saving to', filename
    with open(filename,'w') as f:
        f.write('# time(s) Temp(C)\n')
    return filename
        
def saveData(filename,lastTemp,lastTime):
    # append a single measurement as a single line to a save file
    with open(filename,'a') as f:
        f.write('{0:3f} {1:2.2f}\n'.format(lastTime,lastTemp))

    
def measTemp(exe):
    # do a single measurement using digitemp
    # tell digitemp to save it into a temporary log file
    # read that file into python and delete it
    os.system(exe+' -t0 -ltmp.tmp -q')
    with open('tmp.tmp') as f:
        ll = f.readline().split()
        timeStamp = ll[2]
        temp = float(ll[6])
    os.system('rm tmp.tmp')
    return timeStamp, temp
    
def timeToSec(timeStamp):
    # convert funny time stamp of digitemp into seconds
    tmp = [float(x) for x in timeStamp.split(':')]
    sec = 3600*tmp[0] + 60*tmp[1] + tmp[2]
    return sec
    
def plotInit():
    # initalize figure for plotting
    plt.ion()
    fig = plt.figure()
    ax = fig.add_subplot(1,1,1)
    return fig,ax
    
def plotChart(fig,ax,temps,times,upperThresh,lowerThresh,heat,showLastMinutes):
    timesMinutes = [x / 60.0 for x in times]
    plt.plot(timesMinutes,temps,'C0')
    #axlim = plt.axis()
    plt.plot([timesMinutes[0],timesMinutes[-1]],[upperThresh,upperThresh],'r')
    plt.plot([timesMinutes[0],timesMinutes[-1]],[lowerThresh,lowerThresh],'r')
    if timesMinutes[-1] > showLastMinutes:
        plt.xlim(timesMinutes[-1]-showLastMinutes,timesMinutes[-1])
        #plt.xlim(times[0],times[-1])
    plt.xlabel('t/min')
    plt.ylabel('T/'+degree_sign+'C')
    if heat:
        status = 'heating...'
    else:
        status = 'cooling down...'
    plt.title('aktuell: {0:2.2f} '.format(temps[-1])+degree_sign+'C, '+status)
    fig.canvas.draw()
