# -*- coding: utf-8 -*-
"""
Created on Mon Jan  8 20:05:18 2018

@author: malte
"""
import getpass

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC



degree_sign= u'\N{DEGREE SIGN}'

def webInit(hp):
    # open website of fritzbox powerline to be able to
    # quickly click onto the on/off button
    print 'initializing connection to fritz website...'
    driver = webdriver.Firefox()
    driver.get(hp)
    element = driver.find_element_by_id("uiPass")
    password = getpass.getpass()
    element.send_keys(password)
    element.submit()
    element = WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.ID, "sh"))
        )
    element = driver.find_element_by_id("sh")
    element.click()
    element = WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.ID, "uiSwitch:1000"))
            )
    element = driver.find_element_by_id("uiSwitch:1000")
    switchClass = element.get_attribute("class")
    
    if switchClass == 'switch switch_off':
        heat = False
        print('switch is off')
    elif switchClass == 'switch switch_on':
        heat = True
        print('switch is on')
    else:
        raise Exception('could not determine state of the switch!')
    print 'done initializing website'
    return driver, heat
    
def switchOn(driver,heat):
    # switch the plug on
    if heat:
        print('no need to switch... switch is already on!')
    else:
        print('switching on!')
        element = driver.find_element_by_id("uiSwitch:1000")
        element.click()
        heat = True
    return heat
        
def switchOff(driver,heat):
    # switch the plug off
    if heat == False:
        print('no need to switch... switch is already off!')
    else:
        print('switching off!')
        element = driver.find_element_by_id("uiSwitch:1000")
        element.click()
        heat = False
    return heat
