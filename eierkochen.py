# -*- coding: utf-8 -*-
"""
Created on Sun Jan  7 20:32:42 2018

@author: malte
"""


import time
import auxiliary as aux
import numpy as np


showLastMinutes = 5.0     # plot the last minute of measured data

fitLastMinutes = 1.0

digi = 'digitemp_DS9097'  # executable of digitemp... should be on path
port = '/dev/ttyUSB0'     # serial port in which thermometer is placed
refresh = 1000            # refresh rate in ms... should be bigger than 500
maxLogTime = 120          # measure for maxLogTime minutes, not implemented

aux.init(digi,port,refresh)
fig,ax = aux.plotInit()
saveFileName = aux.initSaveData()



temps = []
times = []
ii = 0
while True:
    timeStamp,temp = aux.measTemp(digi)
    seconds = aux.timeToSec(timeStamp)
    if ii == 0:
        startSec = 1.0*seconds
        
    times.append(seconds-startSec)
    temps.append(temp)

    print 'measured', temps[-1],'C at ',timeStamp
    
    aux.plotChart(fig,ax,temps,times,20.0,20.0,heat,showLastMinutes)
    aux.saveData(saveFileName,temps[-1],times[-1])

    np.polyfit(times, temps, 1)
    
    ii += 1
