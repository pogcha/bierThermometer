This is a python script to perform temperature measurements using a one wire sensor via digitemp.

It plots the temperature consecutively, e.g., for the last minute.

It can also switch the fritz.box powerline network plug on and off for certain threshold temperatures.


The setup which works currently:
The wiring is done as follows (stolen from https://github.com/dword1511/onewire-over-uart)
![Schematics](docs/schaltplan.png "Simplified version")

The real connections are done as in this picture:
![Connection](docs/schaltung.jpg "Connections")
using a high speed diode 1N4148 100V 200mA.

The used sensor is a ds18b20. The USB to serial device is a CP2102 made by SODIAL.

The version of the fritz!OS is 06.92

The version of digitemp is v3.7.1 (compiled for DS9097)
https://www.digitemp.com/software.shtml

The serial port has to have the right persmissions (666). To permenantly set them up add a `50-myusb.rules` and write
```
KERNEL=="ttyUSB[0-9]*",MODE="0666",ATTRS{idVendor}==10c4", ATTRS{idProduct}=="ea60"
```
where you can find the vendor id and product id by hitting `lsusb` and getting something like
```
Bus 003 Device 003: ID 10c4:ea60 Cygnal Integrated Products, Inc. CP210x UART Bridge / myAVR mySmartUSB light
```