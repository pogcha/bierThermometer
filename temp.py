# -*- coding: utf-8 -*-
"""
Created on Sun Jan  7 20:32:42 2018

@author: malte
"""


import time
import auxiliary as aux

# config
upperThresh = 25.0  # if temp is above this threshold... switch plug off
lowerThresh = 22.0  # if temp is below this threshold... switch plug on

digi = 'digitemp_DS9097'  # executable of digitemp... should be on path
port = '/dev/ttyUSB0'     # serial port in which thermometer is placed
refresh = 1000            # refresh rate in ms... should be bigger than 500
maxLogTime = 120          # measure for maxLogTime minutes, not implemented

showLastMinutes = 5.0     # plot the last minute of measured data

hp = "http://192.168.178.26/"  # IP adress of fritz-box powerline

do_not_switch = True          # toggle if plug is switched or not. For True, simply the temperature is measured

# init
if do_not_switch == False:
    import web_aux
    driver, heat = web_aux.webInit(hp)
    time.sleep(30)
    
aux.init(digi,port,refresh)
fig,ax = aux.plotInit()
saveFileName = aux.initSaveData()



temps = []
times = []
ii = 0
while True:
    timeStamp,temp = aux.measTemp(digi)
    seconds = aux.timeToSec(timeStamp)
    if ii == 0:
        startSec = 1.0*seconds
        
    times.append(seconds-startSec)
    temps.append(temp)

    print 'measured', temps[-1],'C at ',timeStamp
    
    
    if temps[-1] < lowerThresh:
        if do_not_switch == False:
            heat = web_aux.switchOn(driver,heat)
        else:
            heat = True
    if temps[-1] > upperThresh:
        if do_not_switch == False:
            heat = web_aux.switchOff(driver,heat)
        else:
            heat = False
    else:
        if do_not_switch == False:
            heat = web_aux.switchOff(driver,heat)
        else:
            heat = False

    aux.plotChart(fig,ax,temps,times,upperThresh,lowerThresh,heat,showLastMinutes)
    aux.saveData(saveFileName,temps[-1],times[-1])

    ii += 1
